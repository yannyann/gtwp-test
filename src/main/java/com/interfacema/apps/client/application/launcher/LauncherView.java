package com.interfacema.apps.client.application.launcher;

import com.google.inject.Inject;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

public class LauncherView extends ViewWithUiHandlers<LauncherUiHandlers> implements LauncherPresenter.MyView {
	
    interface Binder extends UiBinder<Widget, LauncherView> { }

    @Inject
    LauncherView(Binder uiBinder) {
        initWidget(uiBinder.createAndBindUi(this));
    }
    
    @UiField
    TextBox coordinatesTextBox;
    
    @UiField
    TextBox powerTextBox;
    
    @UiField
    Button launchButton;
    
    @UiHandler("launchButton")
    void onLaunchButtonClicked(ClickEvent e) {
    	getUiHandlers().launch(coordinatesTextBox.getText(), powerTextBox.getText());
    }
}
