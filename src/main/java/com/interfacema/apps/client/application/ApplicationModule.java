package com.interfacema.apps.client.application;

import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import com.interfacema.apps.client.application.home.HomeModule;
import com.interfacema.apps.client.application.launcher.LauncherModule;
import com.interfacema.apps.client.application.login.LoginModule;

public class ApplicationModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
    	install(new HomeModule());
    	install(new LauncherModule());
    	install(new LoginModule());

        bindPresenter(ApplicationPresenter.class, ApplicationPresenter.MyView.class, ApplicationView.class, ApplicationPresenter.MyProxy.class);
    }
}
