package com.interfacema.apps.client.application.home;

import com.google.inject.Singleton;
import com.gwtplatform.mvp.client.gin.AbstractPresenterModule;
import com.interfacema.apps.client.application.A;

public class HomeModule extends AbstractPresenterModule {
    @Override
    protected void configure() {
        bindPresenter(HomePresenter.class, HomePresenter.MyView.class, HomeView.class, HomePresenter.MyProxy.class);
        
//        bind(A.class).asEagerSingleton();
        bind(A.class).in(Singleton.class);
    }
}
