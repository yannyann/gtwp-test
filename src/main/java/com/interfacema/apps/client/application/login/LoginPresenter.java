package com.interfacema.apps.client.application.login;

import com.google.gwt.core.shared.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.NoGatekeeper;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.PlaceManager;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.gwtplatform.mvp.shared.proxy.PlaceRequest;
import com.interfacema.apps.client.application.ApplicationPresenter;
import com.interfacema.apps.client.application.CurrentUser;
import com.interfacema.apps.client.place.NameTokens;

public class LoginPresenter extends Presenter<LoginPresenter.MyView, LoginPresenter.MyProxy> implements LoginUiHandlers {
	
	interface MyView extends View, HasUiHandlers<LoginUiHandlers> {
		
	}
	
	@ProxyStandard
	@NameToken(NameTokens.LOGIN)
	@NoGatekeeper
	interface MyProxy extends ProxyPlace<LoginPresenter> {
		
	}
	
	private static final String USERNAME = "user";
	private static final String PASSWORD = "pw";
	
	private CurrentUser currentUser;
	private PlaceManager placeManager;
	
	@Inject
	LoginPresenter(EventBus eventBus, MyView view, MyProxy proxy, CurrentUser currentUser, PlaceManager placeManager) {
		super(eventBus, view, proxy, ApplicationPresenter.SLOT_MAIN);
		this.currentUser = currentUser;
		this.placeManager = placeManager;
		getView().setUiHandlers(this);
	}

	@Override
	public void confirm(String username, String password) {
		if(validateCredentials(username, password)) {
			GWT.log("OK");
			
			currentUser.setLoggedIn(true, username);
			
			PlaceRequest placeRequest = new PlaceRequest.Builder()
					.nameToken(NameTokens.LAUNCHER)
					.build();
			
			GWT.log("LoginPresenter currentUser: " + currentUser.getUsername());

			placeManager.revealPlace(placeRequest);
		} else {
			GWT.log("Wrong username or password.");
		}
	}
	
	private boolean validateCredentials(String username, String password) {
		return username.length() == 4 && password.equals(PASSWORD);
	}

	@Override
	public void showUsername() {
		GWT.log("LoginPresenter currentUser: " + (currentUser!=null ? currentUser.getUsername() : "is null"));
	}

}
