package com.interfacema.apps.client.application;

import com.google.gwt.core.shared.GWT;
import com.google.inject.Singleton;

@Singleton
public class CurrentUser {
	
	public CurrentUser() {
		GWT.log("CurrentUser()");
	}
	
	private boolean isLoggedIn = false;
	private String username = null;

	public void setLoggedIn(boolean isLoggedIn, String username) {
		this.isLoggedIn = isLoggedIn;
		this.username = username;
	}
	
	public String getUsername() {
		return username;
	}

	public boolean isLoggedIn() {
		return isLoggedIn;
	}
}
