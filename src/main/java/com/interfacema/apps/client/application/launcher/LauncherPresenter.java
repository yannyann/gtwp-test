package com.interfacema.apps.client.application.launcher;

import com.google.gwt.core.shared.GWT;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.HasUiHandlers;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.NameToken;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.proxy.ProxyPlace;
import com.interfacema.apps.client.application.ApplicationPresenter;
import com.interfacema.apps.client.application.CurrentUser;
import com.interfacema.apps.client.place.NameTokens;

public class LauncherPresenter extends Presenter<LauncherPresenter.MyView, LauncherPresenter.MyProxy> implements LauncherUiHandlers {
    interface MyView extends View, HasUiHandlers<LauncherUiHandlers> {
    }

    @ProxyStandard
    @NameToken(NameTokens.LAUNCHER)
    interface MyProxy extends ProxyPlace<LauncherPresenter> {
    }
    
    private CurrentUser currentUser;

    @Inject
    LauncherPresenter(EventBus eventBus, MyView view, MyProxy proxy, CurrentUser currentUser) {
        super(eventBus, view, proxy, ApplicationPresenter.SLOT_MAIN);
        view.setUiHandlers(this);
        this.currentUser = currentUser;
    }

	@Override
	public void launch(String coordinates, String power) {
		GWT.log("LauncherPresenter currentUser: " + currentUser.getUsername());
	}
}
