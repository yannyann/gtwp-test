package com.interfacema.apps.client.application.login;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

public class LoginView extends ViewWithUiHandlers<LoginUiHandlers> implements LoginPresenter.MyView {
	
	interface Binder extends UiBinder<Widget, LoginView> { }
	
	@UiField
	Button showUsernameButton;
	
	@UiField
	Button confirmButton;
	
	@UiField
	TextBox usernameTextBox;
	
	@UiField
	PasswordTextBox passwordTextBox;
	
	@Inject
	LoginView(Binder uiBinder) {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@UiHandler("confirmButton")
	void onConfirmButtonClicked(ClickEvent e) {
		getUiHandlers().confirm(usernameTextBox.getText(), passwordTextBox.getText());
	}
	
	@UiHandler("showUsernameButton")
	void onShowUsernameButtonClicked(ClickEvent e) {
		getUiHandlers().showUsername();
	}

}
