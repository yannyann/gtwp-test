package com.interfacema.apps.client.application.launcher;

import com.gwtplatform.mvp.client.UiHandlers;

public interface LauncherUiHandlers extends UiHandlers {
	
	void launch(String coordinates, String power);

}
