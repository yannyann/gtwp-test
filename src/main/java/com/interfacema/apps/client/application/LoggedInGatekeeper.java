package com.interfacema.apps.client.application;

import com.google.gwt.core.shared.GWT;
import com.google.inject.Inject;
import com.gwtplatform.mvp.client.annotations.DefaultGatekeeper;
import com.gwtplatform.mvp.client.proxy.Gatekeeper;

@DefaultGatekeeper
public class LoggedInGatekeeper implements Gatekeeper {
	
	private CurrentUser currentUser;
	
	@Inject
	public LoggedInGatekeeper(CurrentUser currentUser) {
		this.currentUser = currentUser;
	}

	@Override
	public boolean canReveal() {
		GWT.log("Gatekeeper currentUser: " + currentUser.getUsername());
		return currentUser.isLoggedIn();
	}

}
